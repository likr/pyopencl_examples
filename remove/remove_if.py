# coding: utf-8
# vim: filetype=pyopencl.python

from time import time
import numpy
import pyopencl as cl
from pyopencl import array as clarray
from pyopencl.elementwise import ElementwiseKernel
from pyopencl.scan import InclusiveScanKernel
from pyopencl.tools import dtype_to_ctype


def clarray_at(a, position):
    tmp = numpy.empty(1, dtype=a.dtype)
    offset = a.dtype.itemsize * position
    cl.enqueue_copy(a.queue, tmp, a.data, device_offset=offset)
    return tmp[0]


class RemoveIfKernel(object):
    def __init__(self, ctx, dtype, pred):
        ctype = dtype_to_ctype(dtype)
        self.map_kernel = ElementwiseKernel(ctx,
                'int* mark, {}* a'.format(ctype),
                '{} x = a[i]; mark[i] = ({}) == 0;'.format(ctype, pred))
        self.scan_kernel = InclusiveScanKernel(ctx, numpy.uint32, 'a + b', neutral='0')
        self.move_kernel = ElementwiseKernel(ctx,
                '{0}* items, int* mark, int* positions, {0}* result'.format(ctype),
                'if (mark[i] == 1) {result[positions[i] - 1] = items[i];}')

    def __call__(self, a):
        mark = clarray.empty(a.queue, a.shape, numpy.uint32)
        self.map_kernel(mark, a)

        positions = clarray.empty(a.queue, a.shape, numpy.uint32)
        self.scan_kernel(mark, positions)

        size = int(clarray_at(positions, a.size - 1))
        result = clarray.empty(a.queue, size, a.dtype)
        self.move_kernel(a, mark, positions, result)
        return result


def main():
    ctx = cl.create_some_context()
    queue = cl.CommandQueue(ctx)

    remove_if = RemoveIfKernel(ctx, numpy.float32, 'x < 0')

    host_a = numpy.random.rand(10).astype(numpy.float32) * 2 - 1
    a = clarray.to_device(queue, host_a)
    start = time()
    b = remove_if(a)
    stop = time()
    print(stop - start)
    print(b.size == len([x for x in host_a if x >= 0]))
    print(all(b.get() > 0))

if __name__ == '__main__':
    main()
