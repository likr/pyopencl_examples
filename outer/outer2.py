# coding: utf-8
# vim: filetype=pyopencl.python

import pyopencl as cl
from pyopencl import array as clarray
from pyopencl import clrandom
import numpy
import math
from time import time

ctx = cl.create_some_context()
queue = cl.CommandQueue(ctx)

prg = cl.Program(ctx, '''//CL//
__kernel void outer(
        __global const float* a,
        const int a_size,
        __global const float* b,
        const int b_size,
        __global float* c,
        __local float* local_a,
        __local float* local_b,
        const int local_size
)
{
    const int i = get_global_id(0);
    const int j = get_global_id(1);
    if (i < a_size && j < b_size) {
        const int local_i = get_local_id(0);
        const int local_j = get_local_id(1);
        if (local_j == 0) {
            local_a[local_i] = a[local_size * get_group_id(0) + local_i];
        }
        if (local_i == 0) {
            local_b[local_j] = b[local_size * get_group_id(1) + local_j];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        c[i * get_global_size(1) + j] = local_a[local_i] * local_b[local_j];
    }
}
''').build()

size = 64
a = clrandom.rand(queue, size, numpy.float32)
b = clrandom.rand(queue, size, numpy.float32)
c = clarray.empty(queue, (a.size, b.size), numpy.float32)

device = queue.device
max_wg_size = device.max_work_group_size
local_size = min(device.max_work_item_sizes[1], 2 ** int(math.log(math.sqrt(max_wg_size), 2)))
local_a = cl.LocalMemory(local_size)
local_b = cl.LocalMemory(local_size)
x = local_size * (a.size // local_size)
y = local_size * (b.size // local_size)

start = time()
e = prg.outer(queue, (x, y), (local_size, local_size),
        a.data, numpy.int32(a.size), b.data, numpy.int32(b.size), c.data,
        local_a, local_b, numpy.int32(local_size))
e.wait()
stop = time()
print(stop - start)

host_a = a.get()
host_b = b.get()
start = time()
host_c = numpy.outer(host_a, host_b)
stop = time()
print(stop - start)

print(c.get() - host_c)

