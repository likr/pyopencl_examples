# coding: utf-8
# vim: filetype=pyopencl.python

import pyopencl as cl
from pyopencl import array as clarray
from pyopencl import clrandom
import numpy
from time import time

ctx = cl.create_some_context()
queue = cl.CommandQueue(ctx)

prg = cl.Program(ctx, '''//CL//
__kernel void outer(
        __global const float* a,
        __global const float* b,
        __global float* c)
{
    const int i = get_global_id(0);
    const int j = get_global_id(1);
    c[i * get_global_size(1) + j] = a[i] * b[j];
}
''').build()

size = 10000
a = clrandom.rand(queue, size, numpy.float32)
b = clrandom.rand(queue, size, numpy.float32)
c = clarray.empty(queue, (a.size, b.size), numpy.float32)

start = time()
e = prg.outer(queue, c.shape, None, a.data, b.data, c.data)
e.wait()
stop = time()
print(stop - start)

print(c.get() - numpy.outer(a.get(), b.get()))

