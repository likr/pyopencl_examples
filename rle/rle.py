# coding: utf-8
# vim: filetype=pyopencl.python
from time import time
import numpy
import pyopencl as cl
from pyopencl import array as clarray
from pyopencl.scan import ExclusiveScanKernel

kernel_src = '''//CL//
__kernel void boundary_search(
    __global const uchar* a,
    __global int* mark,
    const int size
)
{
    const int i = get_global_id(0);
    mark[i] = (i == size - 1) || (a[i] != a[i + 1]);
}

__kernel void write_result(
    __global const uchar* a,
    __global const int* mark,
    __global const int* positions,
    __global uchar* chars,
    __global int* result,
    const int size
)
{
    const int i = get_global_id(0);
    if (mark[i]) {
        int pos = positions[i];
        chars[pos] = a[i];
        result[pos] = i + 1;
    }
}

__kernel void calculate_length(
    __global const int* result,
    __global int* result2
)
{
    const int i = get_global_id(0);
    if (i == 0) {
        result2[i] = result[i];
    } else {
        result2[i] = result[i] - result[i - 1];
    }
}
'''


def clarray_at(a, position):
    tmp = numpy.empty(1, dtype=a.dtype)
    offset = a.dtype.itemsize * position
    cl.enqueue_copy(a.queue, tmp, a.data, device_offset=offset)
    return tmp[0]


class RunLengthEncoder(object):
    def __init__(self):
        ctx = cl.create_some_context()
        self.queue = cl.CommandQueue(ctx)
        self.prg = cl.Program(ctx, kernel_src).build()
        self.scan_kernel = ExclusiveScanKernel(ctx, numpy.uint32, 'a + b', neutral='0')

    def encode(self, s):
        host_a = numpy.fromstring(s, numpy.uint8)
        a = clarray.to_device(self.queue, host_a)
        mark = clarray.empty(self.queue, a.shape, numpy.uint32)
        positions = clarray.empty(self.queue, a.shape, numpy.uint32)
        size = numpy.int32(a.size)

        self.prg.boundary_search(self.queue, a.shape, None, a.data, mark.data, size)
        self.scan_kernel(mark, positions)

        res_size = int(clarray_at(positions, a.size - 1)) + 1
        res = clarray.empty(self.queue, res_size, numpy.uint32)
        chars = clarray.empty(self.queue, res_size, numpy.uint8)
        self.prg.write_result(self.queue, a.shape, None,
                a.data, mark.data, positions.data, chars.data, res.data, size)

        res2 = clarray.empty(self.queue, res.shape, numpy.uint32)
        self.prg.calculate_length(self.queue, res.shape, None, res.data, res2.data)
        return chars.get(), res2.get()

    def decode(self, s):
        pass


def main():
    encoder = RunLengthEncoder()
    s = 'aaabbccc'
    s = 'aaaaaaaaaaaabbbbbbbbbbccccdeeeeeeeeeefffffffff'

    start = time()
    encoded = encoder.encode(s)
    stop = time()
    print(stop - start)

    print(encoded)

if __name__ == '__main__':
    main()
